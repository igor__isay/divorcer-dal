<?php

namespace DivorcerPackages\DAL\Models\DB;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ApplicationEvents
 * @package App\Models\Database
 *
 * @property $id
 * @property $event_type
 * @property $val1
 * @property $val2
 * @property $val3
 * @property $val4
 * @property $val5
 * @property $val6
 * @property $val7
 * @property $val8
 * @property $val9
 * @property $server
 * @property $is_support
 * @property $time_create
 * @property $console_is_open
 * @property $user_id
 * @property $order_id
 * @property $request
 * @property $result_type
 * @property $client_id
 */
class ApplicationEvents extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    public $primaryKey = 'id';
    protected $table = 'application_events';

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEventType()
    {
        return $this->event_type;
    }

    /**
     * @param mixed $event_type
     */
    public function setEventType($event_type): void
    {
        $this->event_type = $event_type;
    }

    /**
     * @return mixed
     */
    public function getVal1()
    {
        return $this->val1;
    }

    /**
     * @param mixed $val1
     */
    public function setVal1($val1): void
    {
        $this->val1 = $val1;
    }

    /**
     * @return mixed
     */
    public function getVal2()
    {
        return $this->val2;
    }

    /**
     * @param mixed $val2
     */
    public function setVal2($val2): void
    {
        $this->val2 = $val2;
    }

    /**
     * @return mixed
     */
    public function getVal3()
    {
        return $this->val3;
    }

    /**
     * @param mixed $val3
     */
    public function setVal3($val3): void
    {
        $this->val3 = $val3;
    }

    /**
     * @return mixed
     */
    public function getVal4()
    {
        return $this->val4;
    }

    /**
     * @param mixed $val4
     */
    public function setVal4($val4): void
    {
        $this->val4 = $val4;
    }

    /**
     * @return mixed
     */
    public function getVal5()
    {
        return $this->val5;
    }

    /**
     * @param mixed $val5
     */
    public function setVal5($val5): void
    {
        $this->val5 = $val5;
    }

    /**
     * @return mixed
     */
    public function getVal6()
    {
        return $this->val6;
    }

    /**
     * @param mixed $val6
     */
    public function setVal6($val6): void
    {
        $this->val6 = $val6;
    }

    /**
     * @return mixed
     */
    public function getVal7()
    {
        return $this->val7;
    }

    /**
     * @param mixed $val7
     */
    public function setVal7($val7): void
    {
        $this->val7 = $val7;
    }

    /**
     * @return mixed
     */
    public function getVal8()
    {
        return $this->val8;
    }

    /**
     * @param mixed $val8
     */
    public function setVal8($val8): void
    {
        $this->val8 = $val8;
    }

    /**
     * @return mixed
     */
    public function getVal9()
    {
        return $this->val9;
    }

    /**
     * @param mixed $val9
     */
    public function setVal9($val9): void
    {
        $this->val9 = $val9;
    }

    /**
     * @return mixed
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * @param mixed $server
     */
    public function setServer($server): void
    {
        $this->server = $server;
    }

    /**
     * @return mixed
     */
    public function getIsSupport()
    {
        return $this->is_support;
    }

    /**
     * @param mixed $is_support
     */
    public function setIsSupport($is_support): void
    {
        $this->is_support = $is_support;
    }

    /**
     * @return mixed
     */
    public function getTimeCreate()
    {
        return $this->time_create;
    }

    /**
     * @param mixed $time_create
     */
    public function setTimeCreate($time_create): void
    {
        $this->time_create = $time_create;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * @param mixed $order_id
     */
    public function setOrderId($order_id): void
    {
        $this->order_id = $order_id;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param mixed $request
     */
    public function setRequest($request): void
    {
        $this->request = $request;
    }

    /**
     * @return mixed
     */
    public function getResultType()
    {
        return $this->result_type;
    }

    /**
     * @param mixed $result_type
     */
    public function setResultType($result_type): void
    {
        $this->result_type = $result_type;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->client_id;
    }

    /**
     * @param mixed $client_id
     */
    public function setClientId($client_id): void
    {
        $this->client_id = $client_id;
    }

}
