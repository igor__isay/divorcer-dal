<?php

namespace DivorcerPackages\DAL\Repositories;

use DivorcerPackages\DAL\Models\DB\ApplicationEvents;

class ApplicationEventsRepository extends BaseRepository
{
    public static function addEventLog($data)
    {
        $arrayAttr = ['event_type', 'val1', 'val2', 'val3', 'val4', 'val5', 'val6', 'val7', 'val8', 'val9',
            'server', 'is_support', 'time_created', 'user_id', 'order_id', 'request', 'result_type', 'client_id'];

        $applicationEventsObj = new ApplicationEvents();
        foreach ($data as $key => $val) {
            if (in_array($key, $arrayAttr)) {
                $applicationEventsObj->$key = $val;
            }
        }

        return $applicationEventsObj->save();
    }
}
